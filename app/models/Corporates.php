<?php

use Phalcon\Validation;

use Phalcon\Validation\Validator\Regex as RegexValidator;
class Corporates extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $Surname;
public $phone;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("foo");
        $this->setSource("corporates");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'corporates';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Corporates[]|Corporates|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Corporates|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            "id",
            new  Validation\Validator\Digit(
                [
                    "message" => "В поле id должны быть только числовые значения .",
                ]
            )
        );
        $validator->add(
            [
            "phone",
            ],
            new RegexValidator(
                [
                    "pattern" => [
                        "phone"=>"/^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/",

                        ],
                    "message" => ["phone"=>"Введен неверный формат телефона",

                        ]
                ]
            )
        );
//        $validator->add(
//            "name",
//            new  Validation\Validator\Alpha(
//                [
//                    "message" => "В поле имя должны быть только буквенные  значения .",
//                ]
//            )
//        );
        $validator->add(
            "Surname",
            new  Validation\Validator\Alpha(
                [
                    "message" => "В поле фамилия должны быть только буквенные значения .",
                ]
            )
        );
        return $this->validate($validator);
    }
}
