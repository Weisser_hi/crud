<?php


use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class ErrorController extends Controller
{
    public function indexAction()
    {
        $this->view->disableLevel(
            View::LEVEL_MAIN_LAYOUT
        );
        $this->view->code = $this->dispatcher->getParam('code');
        $this->view->message = $this->dispatcher->getParam('message');
    }
}


