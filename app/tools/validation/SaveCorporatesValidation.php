<?php


use Phalcon\Validation\Validator\PresenceOf;

class SaveCorporatesValidation extends \Phalcon\Validation
{

    public function initialize()
    {
        $this->add(
            name,
            new \Phalcon\Validation\Validator\Alpha(
                [
                    'message' => 'Неверное имя',
                ]
            )
        );
    }
}