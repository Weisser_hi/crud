<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class CorporatesController extends Controller
{
    /**
     * Index action
     */
    public function poiskAction()
    {
        $this->persistent->parameters = null;

    }
    public function indexAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Corporates', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $corporates = Corporates::find($parameters);
        if (count($corporates) == 0) {
            $this->flash->notice("При поиске не нашлось ни одной карты");

            $this->dispatcher->forward([
                "controller" => "corporates",
                "action" => "poisk"
            ]);

            return;

        }

        $paginator = new Paginator([
            'data' => $corporates,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }
    /**
     * Searches for corporates
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Corporates', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $corporates = Corporates::find($parameters);
        if (count($corporates) == 0) {
            $this->flash->notice("При поиске не нашлось ни одной карты");

            $this->dispatcher->forward([
                "controller" => "corporates",
                "action" => "poisk"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $corporates,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a corporate
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $corporate = Corporates::findFirstByid($id);
            if (!$corporate) {
                $this->flash->error("Карты не были найдены");

                $this->dispatcher->forward([
                    'controller' => "corporates",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $corporate->id;

            $this->tag->setDefault("id", $corporate->id);
            $this->tag->setDefault("name", $corporate->name);
            $this->tag->setDefault("Surname", $corporate->Surname);
            $this->tag->setDefault("phone", $corporate->phone);
        }
    }

    /**
     * Creates a new corporate
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'poisk'
            ]);

            return;
        }

        $corporate = new Corporates();
        $corporate->name = $this->request->getPost("name");
        $corporate->Surname = $this->request->getPost("Surname");
        $corporate->phone = $this->request->getPost("phone");

        if (!$corporate->save()) {
            foreach ($corporate->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Карта создана успешно");

        $this->dispatcher->forward([
            'controller' => "corporates",
            'action' => 'poisk'
        ]);
    }

    /**
     * Saves a corporate edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $corporate = Corporates::findFirstByid($id);

        if (!$corporate) {
            $this->flash->error("Карты не существует" . $id);

            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'poisk'
            ]);

            return;
        }

        $corporate->name = $this->request->getPost("name");
        $corporate->Surname = $this->request->getPost("Surname");
        $corporate->phone = $this->request->getPost("phone");

        if (!$corporate->save()) {

            foreach ($corporate->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'edit',
                'params' => [$corporate->id]
            ]);

            return;
        }

        $this->flash->success("Карта обновлена успешно");

        $this->dispatcher->forward([
            'controller' => "corporates",
            'action' => 'poisk'
        ]);
    }

    /**
     * Deletes a corporate
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $corporate = Corporates::findFirstByid($id);
        if (!$corporate) {
            $this->flash->error("Карта не была найдена");

            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'index'
            ]);

            return;
        }

        if (!$corporate->delete()) {

            foreach ($corporate->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "corporates",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Карта удалена успешно");

        $this->dispatcher->forward([
            'controller' => "corporates",
            'action' => "index"
        ]);
    }

}
