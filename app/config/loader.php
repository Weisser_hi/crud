<?php

$loader = new \Phalcon\Loader();
$loader->registerNamespaces(
    [
        "crudd\\frontend\\Controllers" => APP_PATH."/controllers",
        "crudd\\frontend\\Models"      => APP_PATH."/models",

        "crudd\\frontend\\tools\\plugins"      => APP_PATH.'/tools/plugins',


    ]
);
/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
);
$loader->registerFiles(
    [
        "../vendor/autoload.php",
    ]
);
$loader->register();